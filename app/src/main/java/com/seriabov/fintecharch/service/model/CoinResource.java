package com.seriabov.fintecharch.service.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

public class CoinResource {
    @NonNull
    public final Status status;
    @Nullable
    public final List<Coin> data;
    @Nullable public final String message;

    private CoinResource(@NonNull Status status, @Nullable List<Coin> data,
                     @Nullable String message) {
        this.status = status;
        this.data = data;
        this.message = message;
    }

    public static CoinResource success(@NonNull List<Coin> data) {
        return new CoinResource(Status.SUCCESS, data, null);
    }

    public static CoinResource error(String msg, @Nullable List<Coin> data) {
        return new CoinResource(Status.ERROR, data, msg);
    }

    public static CoinResource loading(@Nullable List<Coin> data) {
        return new CoinResource(Status.LOADING, data, null);
    }

    public enum Status { SUCCESS, ERROR, LOADING }
}