package com.seriabov.fintecharch.service.repository;

import com.seriabov.fintecharch.service.model.Coin;

import retrofit2.Call;
import retrofit2.http.GET;

import java.util.List;

public interface ApiService {
    String API_URL = "https://api.coinmarketcap.com/v1/";

    @GET("ticker/?limit=20")
    Call<List<Coin>> getCoinsList();
}
