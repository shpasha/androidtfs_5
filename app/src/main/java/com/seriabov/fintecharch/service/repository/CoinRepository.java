package com.seriabov.fintecharch.service.repository;

import android.arch.lifecycle.MutableLiveData;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.seriabov.fintecharch.service.model.CoinResource;
import com.seriabov.fintecharch.service.model.Coin;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CoinRepository {

    private static CoinRepository coinRepository;
    private ApiService apiService;
    private MutableLiveData<CoinResource> data;

    private CoinRepository() {
        data = new MutableLiveData<>();
        //initRetrofit();
    }

    public static CoinRepository getInstance() {
        if (coinRepository == null) {
            coinRepository = new CoinRepository();
        }
        return coinRepository;
    }

    public MutableLiveData<CoinResource> getCoinList() {
        initRetrofit();
        /*
         Если не инициализировать каждый раз, то при переподключении интернета перестаёт  обновление, всегда ошибка.
        Не знаю как сделать по-другому :( */

        data.setValue(CoinResource.loading(null)); //
        /* В докумментации от гугла этот объект создается прям в теле функции, но в таком случае не срабатывает повторное обновелние
         Сделал объект членом класса, надеюсь, это не так плохо. */

        apiService.getCoinsList().enqueue(new Callback<List<Coin>>() {
            @Override
            public void onResponse(Call<List<Coin>> call, Response<List<Coin>> response) {
                data.setValue(CoinResource.success(response.body()));
            }

            @Override
            public void onFailure(Call<List<Coin>> call, Throwable t) {
                data.setValue(CoinResource.error(t.getMessage(), null));
            }
        });
        return data;
    }

    private void initRetrofit() {
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();

        apiService = new Retrofit.Builder()
                .baseUrl(ApiService.API_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(ApiService.class);
    }
}

