package com.seriabov.fintecharch.view;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.seriabov.fintecharch.R;

public class Utils {

    private static Utils utils;
    private Context context;

    public static Utils from(Context context) {
        if (utils == null) {
            utils = new Utils(context);
        }
        return utils;
    }

    private Utils(Context context) {
        this.context = context.getApplicationContext();
    }

    public void loadIcon(String symbol, ImageView logoView) {
        String logoUrl = context.getString(R.string.coin_logo_url, symbol.toLowerCase());
        Glide.with(context)
                .load(logoUrl)
                .into(logoView);
    }

    public void setPercentChange(TextView tv, double percent) {
        tv.setText(context.getString(R.string.percent_format, percent));
        if (percent > 0) {
            tv.setTextColor(ContextCompat.getColor(context, R.color.green700));
        } else {
            tv.setTextColor(ContextCompat.getColor(context, R.color.red700));
        }
    }
}
