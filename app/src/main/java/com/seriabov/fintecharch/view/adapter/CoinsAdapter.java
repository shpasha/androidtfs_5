package com.seriabov.fintecharch.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.seriabov.fintecharch.R;
import com.seriabov.fintecharch.service.model.Coin;
import com.seriabov.fintecharch.view.Utils;
import com.seriabov.fintecharch.view.callback.CoinClickListener;

import java.util.ArrayList;
import java.util.List;

public class CoinsAdapter extends RecyclerView.Adapter<CoinsAdapter.CoinsViewHolder> {

    private List<Coin> items = new ArrayList<>();

    private CoinClickListener coinClickListener;

    public CoinsAdapter(CoinClickListener coinClickListener) {
        this.coinClickListener = coinClickListener;
    }

    @NonNull
    @Override
    public CoinsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_coin, parent, false);
        return new CoinsViewHolder(layout, coinClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull CoinsViewHolder holder, int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setData(List<Coin> newItems) {
        items = newItems;
        notifyDataSetChanged();
    }

    static class CoinsViewHolder extends RecyclerView.ViewHolder {

        Context context;
        CoinClickListener coinClickListener;
        TextView coinName;
        TextView coinPrice;
        TextView coinChange;
        ImageView coinLogo;

        CoinsViewHolder(View itemView, CoinClickListener coinClickListener) {
            super(itemView);
            context = itemView.getContext();
            this.coinClickListener = coinClickListener;
            coinName = itemView.findViewById(R.id.coin_name);
            coinPrice = itemView.findViewById(R.id.coin_price);
            coinChange = itemView.findViewById(R.id.coin_change);
            coinLogo = itemView.findViewById(R.id.coin_logo);

        }

        void bind(Coin info) {
            itemView.setOnClickListener(view -> coinClickListener.onClick(info));

            coinName.setText(info.getName());
            coinPrice.setText(context.getString(R.string.price_format, info.getPriceUsd()));
            coinChange.setText(context.getString(R.string.percent_format, info.getPercentChange7d()));

            Utils.from(context).setPercentChange(coinChange, info.getPercentChange7d());
            Utils.from(context).loadIcon(info.getSymbol(), coinLogo);
        }
    }
}
