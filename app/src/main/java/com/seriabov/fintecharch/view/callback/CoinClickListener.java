package com.seriabov.fintecharch.view.callback;

import com.seriabov.fintecharch.service.model.Coin;

public interface CoinClickListener {
    void onClick(Coin coin);
}
