package com.seriabov.fintecharch.view.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.seriabov.fintecharch.R;
import com.seriabov.fintecharch.service.model.Coin;
import com.seriabov.fintecharch.view.adapter.CoinsAdapter;
import com.seriabov.fintecharch.viewmodel.CoinListViewModel;

import java.util.List;

import timber.log.Timber;

/*
 * TODO:
 * 1) Подключить ViewModel и LiveData из Android Architecture components
 * 2) Разделить классы по пакетам
 * 3) Внедрить в проект архитектуру MVVM, вынести бизнес-логику во вьюмодель.
 * В идеале вьюмодель не должна содержать в себе андроид-компонентов (таких как Context)
 * 4) Сделать так, чтобы при повороте экрана данные не перезапрашивались заново,
 * а использовались полученные ранее
 * 5) Don't repeat yourself - если найдете в коде одинаковые куски кода, выносите в утилитные классы
 */

public class MainActivity extends AppCompatActivity {
    private CoinsAdapter adapter;
    private View errorView, contentView, loadingView;
    private CoinListViewModel coinListViewModel;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);

        errorView = findViewById(R.id.error_layout);
        contentView = findViewById(R.id.main_recycler_view);
        loadingView = findViewById(R.id.loading_layout);

        recyclerView = findViewById(R.id.main_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CoinsAdapter(coin -> DetailsActivity.start(MainActivity.this, coin));
        recyclerView.setAdapter(adapter);

        coinListViewModel = ViewModelProviders.of(this).get(CoinListViewModel.class);

        coinListViewModel.loadCoinList().observe(this, coinsResource -> {
            switch(coinsResource.status) {
                case SUCCESS: {
                    setData(coinsResource.data);
                    break;
                }
                case LOADING: {
                    loadingView.setVisibility(View.VISIBLE);
                    break;
                }
                case ERROR: {
                    showError(coinsResource.message);
                    break;
                }
            }
        });

        fab.setOnClickListener(view -> coinListViewModel.getCoinList());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            getData();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void getData() {
        coinListViewModel.getCoinList();
    }

    private void setData(List<Coin> coins) {
        errorView.setVisibility(View.GONE);
        contentView.setVisibility(View.VISIBLE);
        loadingView.setVisibility(View.GONE);
        adapter.setData(coins);
    }

    private void showError(String error) {
        Timber.d(error);
        contentView.setVisibility(View.GONE);
        loadingView.setVisibility(View.GONE);
        errorView.setVisibility(View.VISIBLE);
    }

}
