package com.seriabov.fintecharch.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.seriabov.fintecharch.service.model.CoinResource;
import com.seriabov.fintecharch.service.repository.CoinRepository;


public class CoinListViewModel extends ViewModel {
    private MutableLiveData<CoinResource> coinListResource;


    public void getCoinList() {
        coinListResource = CoinRepository.getInstance().getCoinList();
    }

    public MutableLiveData<CoinResource> loadCoinList() {
        if (coinListResource == null) {
            getCoinList();
        }
        return coinListResource;
    }
}
